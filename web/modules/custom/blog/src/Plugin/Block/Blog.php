<?php

namespace Drupal\blog\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'My Template' block.
 *
 * @Block(
 *   id = "blog",
 *   admin_label = @Translation("blog")
 * )
 */
class blog extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $renderable = [
      '#theme' => 'blog', 
      '#attached' => [
        'library' => [
          'blog/blog',
        ]
      ]
    ];

    return $renderable;
  }

}
