<?php

/**
 * @file
 * Theme for sexy views.
 */
use Drupal\Component\Utility\Html;
use Drupal\Core\Template\Attribute;
 
function template_preprocess_views_view_tira(&$variables) {
    // Get view options
    $options = $variables['view']->style_plugin->options;
    $variables['options'] = $options;
}