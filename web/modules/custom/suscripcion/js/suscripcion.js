document.querySelector('.subscripcion_form').addEventListener('submit', function(e){
    e.preventDefault();
    let correo_suscriocion = document.querySelector('#correo_suscripcion').value;
    let datosRegistrarSuscrip = {
        'origin':document.title,
        'correo':correo_suscriocion.trim(),     
        'TipoPrograma':'Suscripción',
        'utm_1':getUTM('utm_source'),
        'utm_2':getUTM('utm_medium'),
        'utm_3':getUTM('utm_campaign'),
        'gclid':getUTM('gclid'),
        '_fbp': getCookie('_fbp'),
        '_fbc': getCookie('_fbc') == null ? getUTM('fbclid'):getCookie('_fbc'), 
        'url_source':getCleanUrl(window.location.href) ,
    }

    postLead(datosRegistrarSuscrip).then( data => { 
        console.log(data); 
        document.querySelector('#gracias_subs').style.display = "block";
        document.querySelector('.subscripcion_form').style.display = "none";
        return data} )
    


})