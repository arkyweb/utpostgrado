<?php

namespace Drupal\suscripcion\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'My Template' block.
 *
 * @Block(
 *   id = "suscripcion",
 *   admin_label = @Translation("Suscripcion")
 * )
 */
class Suscripcion extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $renderable = [
      '#theme' => 'suscripcion',
      '#test_var' => 'test variable',
    ];

    return $renderable;
  }

}
