<?php

namespace Drupal\contacto\Plugin\Block;

use Drupal\Core\Block\BlockBase;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;

/**
 * Provides a 'My Template' block.
 *
 * @Block(
 *   id = "contacto",
 *   admin_label = @Translation("contacto")
 * )
 */
class contacto extends BlockBase {

  /**
   * {@inheritdoc}
   */
  private function getNode(){
    $route_match = \Drupal::routeMatch();
    $route_name = $route_match->getRouteName();
    if ($route_name == 'entity.node.canonical') {
      $node = $route_match->getParameter('node');
      $nid_target_node = '';
      $fecha_evento = '';
      $performed_id = '';
      if($node->bundle() === 'article' || $node->bundle() === 'target'){
        $performed_id = $node->get('field_performlead')->value;
      }
      if($node->bundle() === 'article'){
        $fecha_evento = $node->get('field_fecha')->value;
        $nid_target_node =  $node->get('field_programa')->get(0)->getValue()['target_id'];
      }
      if($nid_target_node){
        $target_performed_id = \Drupal::entityTypeManager()->getStorage('node')->load($nid_target_node)->get('field_performlead')->value;
        $target_title = \Drupal::entityTypeManager()->getStorage('node')->load($nid_target_node)->get('field_titulo_url')->value;
      }
      $parametros = [
        'target_performed_id' =>  $nid_target_node ?  $target_performed_id : NULL,
        'target_title'=> $nid_target_node ? $target_title : NUll,
        'performed_id'=> $performed_id ? $performed_id : NUll,
        'fecha_evento' => $fecha_evento ? $fecha_evento : NULL
      ];
      return $parametros ;
    }
  }


  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */

  public function build() {
    $renderable = [
      '#theme' => 'contacto',
      '#performed_id' => $this->getNode()['performed_id'],
      '#target_performed_id'=> $this->getNode()['target_performed_id'],
      '#target_title'=> $this->getNode()['target_title'],
      '#fecha_evento'=> $this->getNode()['fecha_evento']
    ];

    return $renderable;
  }

  /**
   * {@inheritdoc}
   */

  public function getCacheMaxAge() {
    return 0;
  }


}
