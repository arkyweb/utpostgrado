const programasSelect = document.querySelector('#programas_select');
const nombres = document.querySelector('#nombres')
const ap_paterno = document.querySelector('#ap_paterno')
const ap_materno = document.querySelector('#ap_materno')
const dni = document.querySelector('#dni')
const celular = document.querySelector('#celular')
const correo = document.querySelector('#email')
const docs_select = document.querySelector('#docs_select')
const CiudadResidencia = document.querySelector('#ciudad')

const form = document.querySelector('#contacto_form');
const fetchdataJSON = async(url)=>{
    const response = await fetch(url);
    const data = await response.json();
    return data;
}
const urlGetCarreras = `/get/productos`;
fetchdataJSON(urlGetCarreras).then(data => {
    let programasSort = data.sort(compare);
    let listProgramasGrupoTipo = programasSort.reduce(function (h, obj) {
        h[obj.tipo] = (h[obj.tipo] || []).concat(obj);
        return h;
    }, {})
    var Facultades = Object.keys(listProgramasGrupoTipo)
        for (var f = 0; f < Facultades.length; f++) {
        optgroup = document.createElement('optgroup');
        optgroup.label = Facultades[f];
        cadaFacultad = listProgramasGrupoTipo[ Facultades[f] ]
        cadaFacultad = cadaFacultad.sort(compareP);
        for (var c = 0; c < cadaFacultad.length; c++) {
            option = document.createElement('option');
            option.text = cadaFacultad[c].field_titulo_url;
            option.value = cadaFacultad[c].performlead;
            option.setAttribute('tipo', cadaFacultad[c].tipo);
            optgroup.appendChild(option);
        }
            programasSelect.add(optgroup);
    }
    let url = window.location.href;
});

const titleForm = document.querySelector('#contacto > div > div.modal-body > div > h5');
//Cambio de titulo segun click al boton
setTimeout(() => {
    if(document.querySelector('.button-contacto')){
        document.querySelector('.button-contacto').addEventListener('click',function(e){
            titleForm.innerHTML = 'Contáctanos';
            programasSelect.parentNode.style.display = 'block';
            programasSelect.selectedIndex = 0;
            if(document.querySelector('#appends')){
                document.querySelector('#appends').style.display='none';
            }
        })
    }
    if(document.querySelector('.open-contacto-eventos')){

        document.querySelectorAll('.open-contacto-eventos').forEach(element => {
            element.addEventListener('click',function(e){
                titleForm.innerHTML = 'Solicita más información';
                programasSelect.parentNode.style.display = 'none';
                programasSelect.selectedIndex = 2;
                if(document.querySelector('#appends')){
                    document.querySelector('#appends').style.display='block';
                }
            })
        });

        // document.querySelector('.open-contacto-eventos').addEventListener('click',function(e){
        //     titleForm.innerHTML = 'Solicita más información';
        //     programasSelect.parentNode.style.display = 'none';
        //     programasSelect.selectedIndex = 2;
        //     if(document.querySelector('#appends')){
        //         document.querySelector('#appends').style.display='block';
        //     }
        // })


    }

    if(document.querySelector('.open-contacto-preselect')){
        document.querySelector('.open-contacto-preselect').addEventListener('click',function(e){
            titleForm.innerHTML = 'Solicita más información';
            programasSelect.parentNode.style.display = 'none';
            programasSelect.value = document.querySelector('.performlead').innerHTML.trim();
            if(document.querySelector('#appends')){
                document.querySelector('#appends').style.display='none';
            }

        })
    }

    if(document.querySelector('#gracias_contacto') && document.querySelector('#appends')){
        document.querySelector('#gracias_contacto').appendChild(document.querySelector('#appends'))
    }

}, 2500);


//Validaciones en el tipeo
document.querySelectorAll('#contacto_form input').forEach(element => {
        element.addEventListener('input', function(element){
            console.log(element)
             validar(this)
        }  )
});
//Validaciones en el select
document.querySelectorAll('#contacto_form select').forEach(element => {
    element.addEventListener('change', function(element){
        console.log(element)
         validar(this)
    }  )
});

//Seleccion de tipo de documento
docs_select.addEventListener('change', function(e){
    switch (this.value) {
        case "dni":
                dni.setAttribute('maxlength', 8);
            break;
        case "ce":
             dni.setAttribute('maxlength', 12);
            break;
        case "ci":
                dni.setAttribute('maxlength', 12);
            break;
    }
})
form.addEventListener('submit', function(e){
    e.preventDefault();
    let comodin = []
    document.querySelectorAll('#contacto_form input , #contacto_form select').forEach(element => {
            validar(element);
            if(element.parentNode.classList.contains('form-error')){
                    comodin.push(element)
            }
        });
    if (comodin.length === 0) {
        let datosRegistrar = {
        'origin':document.title,
        'nombres':nombres.value,
        'apellidoPaterno':ap_paterno.value,
        'apellidoMaterno':ap_materno.value,
        'nombres': nombres.value.trim(),
        'dni': dni.value.trim(),
        'tipoDocumento':docs_select.value.trim(),
        'celular':celular.value.trim(),
        'CiudadResidencia':CiudadResidencia.value.trim(),
        'correo':correo.value.trim(),
        'Programa': window.location.href.includes('eventos')?document.querySelector('.target_title').innerHTML.trim():getTextSelect(programasSelect).trim(),
        'IDperformedLead':window.location.href.includes('eventos')?document.querySelector('.target_performed_id').innerHTML.trim(): programasSelect.value.trim(),
        'TipoPrograma':window.location.href.includes('eventos')?'evento':getCustonAtribute(programasSelect,'tipo').trim(),
        'programa_evento': window.location.href.includes('eventos')?document.querySelector('#title > span').innerHTML.trim():null,
        'programa_evento_id': window.location.href.includes('eventos')?document.querySelector('.performlead').innerHTML.trim():null,
        'FechaEvento':window.location.href.includes('eventos')?document.querySelector('.fecha_evento').innerHTML.trim():null,
        'utm_1':getUTM('utm_source'),
        'utm_2':getUTM('utm_medium'),
        'utm_3':getUTM('utm_campaign'),
        'gclid':getUTM('gclid'),
        '_fbp': getCookie('_fbp'),
        '_fbc': getCookie('_fbc') == null ? getUTM('fbclid'):getCookie('_fbc'),
        'url_source':getCleanUrl(window.location.href) ,
    }
    console.log(datosRegistrar);
    postLead(datosRegistrar).then( data => {
        console.log(data);
        document.querySelector('#gracias_contacto').style.display = "block";
        form.style.display = "none";
        titleForm.style.display = "none";
        return data} )
    }
})

const postLead = async (datos) => {
      const settings = {
        method: 'POST',
        body: JSON.stringify(datos),
        headers: {
            'Content-Type': 'application/json'
        }
    };
    try {
        const fetchResponse = await fetch(`/customers-api/post-leads`, settings);
        const data = await fetchResponse.json();
        return data;
    } catch (e) {
        return e;
    }

  }

//codigo de validacion
const validar = (target) => {
  const regexABC = /^[a-zA-ZÀ-ÿ\u00f1\u00d1\s]+$/gi;
  const space = /^[\s]+$/gi;
  const regexMail = /\b[\w]+@[\w-]+(?:\.[\w]+)+\b/;
      switch (target.type) {
          case 'text':
              if (target.value.match(regexABC) === null) {
                  onlyText = target.value.replace(/[^a-zA-ZÀ-ÿ\u00f1\u00d1\s]/gi, '');
                  target.value = onlyText;
              }
              //No permite poner puros espacios en el input
              if (target.value.match(space)) {
                  onlyText = target.value.replace(/[\s]/gi, '');
                  target.value = onlyText;
              }
              //Validar campos con caracteres
              if (target.value.length === 0 ) {
                  target.parentNode.classList.add('form-error');
              } else {
                  target.parentNode.classList.remove('form-error')
              }
              break;
          case 'tel':
              var onlyNumbers;
              //Elimina caracteres especiales letras y espacios
              if (target.value.match(/[a-zA-ZÀ-ÿ\u00f1\u00d1\W\s]/gi)) {
                  onlyNumbers = target.value.replace(/[a-zA-ZÀ-ÿ\u00f1\u00d1\W\s]/gi, '');
                  target.value = onlyNumbers;
              }
              if (target.value.length < target.maxLength) {
                  target.parentNode.classList.add('form-error');
              } else {
                  target.parentNode.classList.remove('form-error')
              }
              break;
            case 'select-one':
              if(target.value.trim().length === 0){
                console.log('selecciona un valor');
                target.parentNode.classList.add('form-error')
              }else{
                target.parentNode.classList.remove('form-error')
              }
            break;
          case 'email':
              var onlyEmail;
              //correo correcto
              if (target.value.match(regexMail)) {
                target.parentNode.classList.remove('form-error')
              }
              //correo incorrecto
              if (target.value.match(regexMail) === null) {
                target.parentNode.classList.add('form-error')
              }
            break;
        case 'checkbox':
             if(target.checked){
                target.parentNode.classList.remove('form-error')
             }else{
                target.parentNode.classList.add('form-error')
             }
            break;
      }
  }
  //codigo de UTM
  function getUTM(name){
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }
  //Obtebción de la cookie
  function getCookie(name) {
  // Split cookie string and get all individual name=value pairs in an array
  var cookieArr = document.cookie.split(";");
  // Loop through the array elements
  for(var i = 0; i < cookieArr.length; i++) {
      var cookiePair = cookieArr[i].split("=");
      /* Removing whitespace at the beginning of the cookie name
      and compare it with the given string */
      if(name == cookiePair[0].trim()) {
          // Decode the cookie value and return
          return decodeURIComponent(cookiePair[1]);
      }
  }
  // Return null if not found
  return null;
  }
  //Obtener el texto de una opcion seleccionada
  function getTextSelect(sel) {
    return sel.options[sel.selectedIndex].text;
  }
  function getCustonAtribute(sel,atribute){
    return sel.options[sel.selectedIndex].getAttribute(atribute);
  }
  const getCleanUrl = function(url) {
  return url.replace(/#.*$/, '').replace(/\?.*$/, '');
  };

/**Funciones para ordenar alfabeticmente */
function compare(a, b) {
    const genreA = a.tipo.toUpperCase();
    const genreB = b.tipo.toUpperCase();

    var comparison = 0;
    if (genreA > genreB) {
        comparison = 1;
    } else if (genreA < genreB) {
        comparison = -1;
    }
    return comparison;
}

function compareP(a, b) {
    const genreA = a.field_titulo_url.toUpperCase();
    const genreB = b.field_titulo_url.toUpperCase();

    var comparison = 0;
    if (genreA > genreB) {
        comparison = 1;
    } else if (genreA < genreB) {
        comparison = -1;
    }
    return comparison;
}

switch (document.readyState) {
    case "loading":
      // The document is still loading.
      console.log('The document is still loading.');
      break;
    case "interactive":
      // The document has finished loading. We can now access the DOM elements.
    console.log('The document has finished loading. We can now access the DOM elements.');
      break;
    case "complete":
    console.log('The page is fully loaded.');

    //   console.log("The first CSS rule is: " + document.styleSheets[0].cssRules[0].cssText);
      break;
  }
