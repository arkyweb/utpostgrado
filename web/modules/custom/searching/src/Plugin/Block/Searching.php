<?php

namespace Drupal\searching\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'My Template' block.
 *
 * @Block(
 *   id = "searching",
 *   admin_label = @Translation("searching")
 * )
 */
class searching extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $renderable = [
      '#theme' => 'searching',
      '#test_var' => 'test variable',
    ];

    return $renderable;
  }

}
