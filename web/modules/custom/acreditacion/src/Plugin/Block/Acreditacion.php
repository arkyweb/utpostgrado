<?php

namespace Drupal\acreditacion\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'My Template' block.
 *
 * @Block(
 *   id = "acreditacion",
 *   admin_label = @Translation("acreditacion")
 * )
 */
class acreditacion extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $renderable = [
      '#theme' => 'acreditacion',
      '#attached' => [
        'library' => [
          'acreditacion/acreditacion',
        ]
      ]
    ];

    return $renderable;
  }

}
