"use strict";

fetch(XML_URL_A)
    .then(
        response => response.text()
    )
    .then(
        str => new window.DOMParser().parseFromString(str, "text/xml")
    )
    .then(
        data => {
            const items = data.querySelectorAll("item");
            let html = ``;
            for (var i=0; i<3; i++) {
                //console.log(items[i].innerHTML);
                const el = items[i];
                const el_title = el.querySelector("title").innerHTML;
                const el_link = el.querySelector("link").innerHTML;
                const el_desc = el.querySelector("description").innerHTML;
                let el_image = null;
                let encoded = new window.DOMParser().parseFromString(
                    el.getElementsByTagNameNS("*", "encoded").item(0).innerHTML,
                    "text/html"
                );
                if (encoded.querySelector('img') != null) {
                    el_image = encoded.querySelector('img').src;
                } else {
                    return true;
                };
                html += `
                <div class="base">
                    <figure>
                        <img data-src="${el_image}" alt="${el_title}" loading="lazy" uk-img width="528" height="224">
                    </figure>
                    <article>
                        <h4>${el_title}</h4>
                        <div>${ el_desc }</div>
                        <div class="contents_cta">
                            <a href="${ el_link }" target="_blank" class="action">
                                <span class="flecha svgs_arrow_bold">
                                    <svg class="icon icon--utp_arrow_bold">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#utp_arrow_bold"></use>
                                    </svg>
                                </span>
                                Leer más
                            </a>
                        </div>
                    </article>
                </div>
                `;
            }
            document.getElementById('the_blog').insertAdjacentHTML("beforeend", html);
        }
    );
