<?php

namespace Drupal\cursos\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'My Template' block.
 *
 * @Block(
 *   id = "cursos",
 *   admin_label = @Translation("cursos")
 * )
 */
class cursos extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $renderable = [
      '#theme' => 'cursos', 
      '#attached' => [
        'library' => [
          'cursos/cursos',
        ]
      ]
    ];

    return $renderable;
  }

}
