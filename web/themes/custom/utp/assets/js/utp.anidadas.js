function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  }
  let stroke_svg = document.querySelector('.tipo_anidadas');
  let stroke_donde = document.querySelector('.trio_items:last-child .block_content');
  insertAfter(stroke_donde, stroke_svg);