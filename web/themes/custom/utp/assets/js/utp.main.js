let e = document.querySelectorAll('.modal_menu-desktop .uk-modal-body');
Array.from(e).forEach(function (i) {
    let k = document.querySelector('.modal-footer').cloneNode(true);
    i.append(k); k.removeAttribute('style');
});
document.querySelector('.modal-footer[style]').remove();

const videoPointers = document.querySelectorAll(".action_click");
window.addEventListener("load", () => {
    for (const videoPointer of videoPointers) {
        videoPointer.addEventListener("click", videoClick); 
    };
});
function videoClick () {
    for (let i = 0; i < videoPointers.length;  i++) {
        videoEmbed = document.querySelectorAll(".video_modal iframe");
        if (videoPointers[i].classList.contains('action_click')) {
            for (let i = 0; i < videoEmbed.length;  i++) {
                const videoSrc = videoEmbed[i].getAttribute('data-src');
                videoEmbed[i].setAttribute("src", videoSrc); 
                videoPointers[i].classList.remove("action_click");
            };
        }
    }
};