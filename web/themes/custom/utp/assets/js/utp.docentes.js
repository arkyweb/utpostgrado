function getUTM2(name){
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }


const fetchdataJSON_2 = async(url)=>{
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

if(document.querySelector('#edit-programa--2')){
    const urlGetCarreras2 = `/get/productos`;
    const selectFiltroDocentes = document.querySelector('#edit-programa--2');
    selectFiltroDocentes.innerHTML = '<option value="All" selected="selected">Todos</option>';
    fetchdataJSON_2(urlGetCarreras2).then(data => {
        let programasSort = data.sort(compare);
        let listProgramasGrupoTipo = programasSort.reduce(function (h, obj) {
            h[obj.tipo] = (h[obj.tipo] || []).concat(obj);
            return h;
        }, {})
        var Facultades = Object.keys(listProgramasGrupoTipo)
            for (var f = 0; f < Facultades.length; f++) {
            optgroup = document.createElement('optgroup');
            optgroup.label = Facultades[f];
            cadaFacultad = listProgramasGrupoTipo[ Facultades[f] ]
            cadaFacultad = cadaFacultad.sort(compareP);
            for (var c = 0; c < cadaFacultad.length; c++) {
                option = document.createElement('option');
                option.text = cadaFacultad[c].field_titulo_url;
                option.value = cadaFacultad[c].id;
                optgroup.appendChild(option);
            }
            selectFiltroDocentes.add(optgroup);
        }
    }).then((j)=>{
        selectFiltroDocentes.addEventListener('change', function(e){
            window.location.href = `/docentes?programa=${this.value}`;

        })

        if(getUTM2("programa").length > 0){
            //console.log(getUTM2("programa"));
            selectFiltroDocentes.value = getUTM2("programa");
        }
    });
};

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
let stroke_svg = document.querySelector('.tipo_docentes');
let stroke_donde = document.querySelector('.trio_items:last-child .block_content');
insertAfter(stroke_donde, stroke_svg);
