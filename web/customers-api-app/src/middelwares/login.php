<?php

$usuarios =   function($request,  $response, $next ){
    $route = $request->getAttribute('route');
    $arguments = $route->getArguments();
    $correo = $arguments['email'];
    $pass = $arguments['pass'];
    $sql = "SELECT user, pass FROM usuarios where user = '$correo'";
    try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->query($sql);
        if($result->rowCount()>0){
            $registros= $result->fetchAll(PDO::FETCH_OBJ);
            if (password_verify($pass, $registros[0]->pass)) {
                $response = $next($request, $response);
            }else {
                $response->getBody()->write(json_encode('No tienes autorización'));
            }
        }else{
            $response->getBody()->write(json_encode('no hay registros'));
        }

        $result = null;
        $registros = null;

    } catch (PDOException $e) {
        $response->getBody()->write( $e->getMessage() );
    }

    return $response;
};
