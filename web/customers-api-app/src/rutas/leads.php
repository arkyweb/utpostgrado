<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app = new \Slim\App;


$app->get('/customers-api/get-leads-v/{email}/{pass}[/{id}]', function(Request $request, Response $response ){
    $ID = $request->getAttribute('id');
    // $inicial = $request->getAttribute('inicial') . ' 00:00:00';
    // $final = $request->getAttribute('final') . ' 23:59:59';

    if ($ID) {
        $sql = "SELECT * FROM leads_utp WHERE id = $id";
    }else{
        $sql = "SELECT id, 
            apellidoPaterno,
            apellidoMaterno,
            nombres,
            tipoDocumento,
            dni,
            celular,
            correo,
            Programa,
            IDperformedLead,
            TipoPrograma,
            programa_evento,
            programa_evento_id,
            FechaEvento,
            CiudadResidencia,
            utm_1,
            utm_2,
            utm_3,
            gclid,
            url_source,
        dateCreated,
        dateSendCRM,
        registroLogSimple, registroLog FROM leads_utp ORDER BY ID DESC LIMIT 1000";
    }
   
    try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->query($sql);
        if($result->rowCount()>0){
            $registros= $result->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write( json_encode( $registros, JSON_UNESCAPED_UNICODE ) );
        }else{
            $response->getBody()->write( json_encode('no hay registros') );
        }
        $result = null;
        $registros = null;
    } catch (PDOException $e) {
        $response->getBody()->write( $e->getMessage() );    
    }

})->add($usuarios);


$app->get('/customers-api/get-leads-rango/{email}/{pass}/{inicial}/{final}', function(Request $request, Response $response ){
        $inicial = $request->getAttribute('inicial') . ' 00:00:00';
        $final = $request->getAttribute('final') . ' 23:59:59';
        $sql = "SELECT id, 
        apellidoPaterno,
        apellidoMaterno,
        nombres,
        tipoDocumento,
        dni,
        celular,
        correo,
        Programa,
        IDperformedLead,
        TipoPrograma,
        programa_evento,
        programa_evento_id,
        FechaEvento,
        CiudadResidencia,
        utm_1,
        utm_2,
        utm_3,
        gclid,
        url_source,
        dateCreated,
        dateSendCRM,
        registroLogSimple FROM leads_utp WHERE dateCreated BETWEEN '$inicial' AND '$final'";
        try {
            $db = new db();
            $db = $db->conectDB();
            $result = $db->query($sql);
            if($result->rowCount()>0){
                $registros= $result->fetchAll(PDO::FETCH_OBJ);
                $response->getBody()->write( json_encode( $registros, JSON_UNESCAPED_UNICODE ) );

            }else{
                $response->getBody()->write( json_encode('no hay registros en las fechas indicadas '.$sql ) );
            }

            $result = null;
            $registros = null;

        } catch (PDOException $e) {
            echo '{"error": {"text": }'.$e->getMessage().'}';       
        }
})->add($usuarios);

$app->post('/customers-api/post-leads', function(Request $request, Response $response ){
   $origin = $request->getParam('origin');
   $nombres = $request->getParam('nombres');
   $apellidoPaterno = $request->getParam('apellidoPaterno');
   $apellidoMaterno = $request->getParam('apellidoMaterno');
   $dni = $request->getParam('dni');
   $celular = $request->getParam('celular');
   $correo = $request->getParam('correo');
   $Programa = $request->getParam('Programa');
   $IDperformedLead = $request->getParam('IDperformedLead');
   $TipoPrograma = $request->getParam('TipoPrograma');

   $programa_evento = $request->getParam('programa_evento');
   $programa_evento_id = $request->getParam('programa_evento_id');
   $FechaEvento = $request->getParam('FechaEvento');



   $tipoDocumento = $request->getParam('tipoDocumento');
   $CiudadResidencia = $request->getParam('CiudadResidencia');
   $utm_1 = $request->getParam('utm_1');
   $utm_2 = $request->getParam('utm_2');
   $utm_3 = $request->getParam('utm_3');
   $gclid = $request->getParam('gclid');
   $url_source = $request->getParam('url_source');
   date_default_timezone_set('America/Lima');
   $dateCreated = date('Y-m-d H:i:s'); 
   $sql = "INSERT INTO leads_utp
                        (
                            apellidoPaterno,
                            apellidoMaterno,
                            nombres,
                            dni,
                            celular,
                            correo,
                            Programa,
                            IDperformedLead,
                            TipoPrograma,
                            tipoDocumento,
                            programa_evento,
                            programa_evento_id,
                            FechaEvento,
                            CiudadResidencia,
                            utm_1,
                            utm_2,
                            utm_3,
                            origin,
                            gclid,
                            url_source,
                            dateCreated
                        ) 
                        VALUES
                        (
                            :apellidoPaterno,
                            :apellidoMaterno,
                            :nombres,
                            :dni,
                            :celular,
                            :correo,
                            :Programa,
                            :IDperformedLead,
                            :TipoPrograma,
                            :tipoDocumento,
                            :programa_evento,
                            :programa_evento_id,
                            :FechaEvento,
                            :CiudadResidencia,
                            :utm_1,
                            :utm_2,
                            :utm_3,
                            :origin,
                            :gclid,
                            :url_source,
                            :dateCreated
                        )";

            try {
                    $db = new db();
                    $db = $db->conectDB();
                    $result = $db->prepare($sql);
                    $result->bindParam(':apellidoPaterno', $apellidoPaterno);
                    $result->bindParam(':apellidoMaterno', $apellidoMaterno);
                    $result->bindParam(':nombres', $nombres);
                    $result->bindParam(':dni', $dni);
                    $result->bindParam(':celular', $celular);
                    $result->bindParam(':correo', $correo);
                    $result->bindParam(':Programa', $Programa);
                    $result->bindParam(':IDperformedLead', $IDperformedLead);
                    $result->bindParam(':TipoPrograma', $TipoPrograma);
                    $result->bindParam(':programa_evento', $programa_evento);
                    $result->bindParam(':programa_evento_id', $programa_evento_id);
                    $result->bindParam(':FechaEvento', $FechaEvento);
                    $result->bindParam(':tipoDocumento', $tipoDocumento);
                    $result->bindParam(':CiudadResidencia', $CiudadResidencia);
                    $result->bindParam(':utm_1', $utm_1);
                    $result->bindParam(':utm_2', $utm_2);
                    $result->bindParam(':utm_3', $utm_3);
                    $result->bindParam(':origin', $origin);
                    $result->bindParam(':gclid', $gclid);
                    $result->bindParam(':url_source', $url_source);
                    $result->bindParam(':dateCreated', $dateCreated);
                    $result->execute();
                    echo json_encode($db->lastInsertId());
                    $result = null;
                    $registros = null;
                } catch (PDOException $e) {
                    echo '{"error": {"text": }'.$e->getMessage().'}';
                }

});

$app->get('/customers-api/post-leads-crm', function(Request $request, Response $response ){
    function updateLead($id, $response, $flagSimple ){
        date_default_timezone_set('America/Lima');
        $sql = " UPDATE leads_utp SET dateSendCRM= :dateSendCRM , registroLog= :registroLog , registroLogSimple= :registroLogSimple  WHERE id= :id ";
        try {

            $dateSendVariable = date('Y-m-d H:i:s');
            $db = new db();
            $db = $db->conectDB();
            $result = $db->prepare($sql);
            $result->bindParam(':id', $id);
            $result->bindParam(':dateSendCRM', $dateSendVariable );
            $result->bindParam(':registroLog', $response );
            $result->bindParam(':registroLogSimple', $flagSimple );
            $result->execute();
            echo json_encode('Lead actualizado luego de su intento de envio');
            $result = null;
            $registros = null;
        } catch (PDOException $e) {
            echo '{"error": {"text": }'.$e->getMessage().'}';
        }
    };

    function sendCRM($arrObjt){

        $parameters = [];
        $base_url = 'https://utp.performlead.com/api/contacto/informes?formulario=';

        switch ($arrObjt->TipoPrograma) {
            case 'evento':
                $parameters=[
                    'origin'=>$arrObjt->origin,
                    'page_source' => "https://www.postgradoutp.edu.pe/",
                    'lastname' => $arrObjt->apellidoPaterno.' '.$arrObjt->apellidoMaterno,
                    'fullname' => $arrObjt->nombres,
                    'dni'     => $arrObjt->dni,
                    'email'   => $arrObjt->correo,
                    'celular' => $arrObjt->celular,
                    'ciudad' => $arrObjt->CiudadResidencia,
                    'terminos' => 1,
                    'politicas' => 1,
                    'nombre_evento' => $arrObjt->programa_evento,
                    'FechaEvento' => $arrObjt->FechaEvento,//DOnde
                    'programa' => $arrObjt->Programa, //El titulo del programa
                    'id_programa' => $arrObjt->IDperformedLead,//urlprefijo del tipo de contenido cambiar
                    'programa_evento' => $arrObjt->programa_evento,//string
                    'programa_evento_id' => $arrObjt->programa_evento_id,//IDperformedLead
                    'es_curso' => null,//le paso null
                    'maestria_especializacion_curso' => null,//le paso null
                    'mobile_data' =>$arrObjt->id,//el id de tabla 
                    "utm_source"=>$arrObjt->utm_1,
                    "utm_medium"=>$arrObjt->utm_2,
                    "utm_campaign"=>$arrObjt->utm_3,
                    'conferencia1' => $arrObjt->programa_evento,//string titulo del evento
                    'conferencia2' => ''//nulo
                ];
                //Envio Eventos
                $urlFinal =  $base_url.$arrObjt->programa_evento_id;
                break;
            case 'Suscripción':
                $parameters = [
                    "email"=> $arrObjt->correo,
                    "eventName"=>"reto-transformacion-digital-mba",
                    "attributes"=>"{}"
                ];
                break;
            default:
                $parameters = [
                    "origin"=>$arrObjt->origin,
                    "page_source"=>"https://www.postgradoutp.edu.pe/",
                    "fullname"=>$arrObjt->nombres,
                    "lastname"=>$arrObjt->apellidoPaterno.' '.$arrObjt->apellidoMaterno,
                    "dni"=>$arrObjt->dni,
                    "email"=>$arrObjt->correo,
                    "celular"=>$arrObjt->celular,
                    "terminos"=>1,
                    "ciudad"=>$arrObjt->CiudadResidencia,
                    "politicas"=>1,
                    "phonecasa"=>000,
                    "maestria_especializacion_curso"=>$arrObjt->Programa,
                    "utm_source"=>$arrObjt->utm_1,
                    "utm_medium"=>$arrObjt->utm_2,
                    "utm_campaign"=>$arrObjt->utm_3,
                    "leadId"=>$arrObjt->id,
                    "tipo_de_documento"=>$arrObjt->tipoDocumento,
                    "clientId"=>$arrObjt->gclid
                ];
                //Envio Cursos
                $urlFinal =  $base_url.$arrObjt->IDperformedLead;
                break;
        }

        if($arrObjt->TipoPrograma !== 'Suscripción'){
            $fields = http_build_query($parameters);
            $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
            $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=utf-8';
            $headers[] = 'X-Requested-With: XMLHttpRequest';
            $headers[] = 'Origin';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL,$urlFinal);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$fields);
            $result=curl_exec($ch);
            curl_close($ch);
            updateLead($arrObjt->id, $result, 'Enviado' );
        }else{
            $curl = curl_init();

curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://track.embluemail.com/contacts/event',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($parameters),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic YjVjZTgzOWViZWFlNGVmNGI0MDNmNTQxOWU2YWI0ZjM='
            ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            updateLead($arrObjt->id, $response, 'Enviado' );
            //echo $response;
        }



    }

    $sql = "SELECT * FROM leads_utp WHERE dateSendCRM IS  NULL LIMIT 10";

    try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->query($sql);
        if($result->rowCount()>0){
            $registros = $result->fetchAll(PDO::FETCH_OBJ);
            //print_r($registros);
            for ($i=0; $i < $result->rowCount() ; $i++) { 
                sendCRM($registros[$i]);
            }
    
        }else{
            echo json_encode('no hay registros');
        }

        $result = null;
        $registros = null;

    } catch (PDOException $e) {
        echo '{"error": {"text": }'.$e->getMessage().'}';
    }
});


$app->get('/customers-api/get-feed', function(Request $request, Response $response ){
    $fileContents= file_get_contents('https://www.postgradoutp.edu.pe/blog/feed/');
    $a = simplexml_load_string($fileContents);
    echo json_encode($a,JSON_PRETTY_PRINT);
});