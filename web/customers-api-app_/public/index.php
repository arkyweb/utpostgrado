<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

$config = ['settings' => [
    'determineRouteBeforeAppMiddleware' => true,
    'debug' => true
]];

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../src/config/db.php';
$app = new \Slim\App($config);
require __DIR__ . '/../src/middelwares/login.php';
require __DIR__ . '/../src/rutas/leads.php';

// Run app
$app->run();

